
let student = []
// 1.
function addStudent (element){
	student.push(element)
}
addStudent("Art");
addStudent("Bert");
addStudent("Mark");

// 2.
function printStudent (){
	for(let x = 0; x < student.length; x++)
		console.log(student[x])
}
printStudent()

// 3.
function countStudent(element){
	console.log(element)
}
countStudent(student.length)

// 4.
function findStudent(element){
	if(student.includes(element)){
		return element
	} else {
		return `${element} not found`
	}
}
console.log(findStudent("Art"))